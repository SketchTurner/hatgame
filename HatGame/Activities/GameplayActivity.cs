using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Preferences;

using HatGame.Fragments;

namespace HatGame.Activities
{
    [Activity(ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class GameplayActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Gameplay);

            var teamFragment = new CurrentTeamFragment();
            var fTrans = FragmentManager.BeginTransaction();
            fTrans.Add(Resource.Id.GameplayFragmentContainer, teamFragment);
            fTrans.Commit();
        }

        public override void OnBackPressed()
        {
            var fragment = FragmentManager.FindFragmentByTag("results");

            if (fragment != null)
            {
                Finish();
                return;
            }

            base.OnBackPressed();
        }
    }
}