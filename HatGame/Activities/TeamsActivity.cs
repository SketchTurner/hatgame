using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Database;
using Android.Preferences;
using System.Threading;

namespace HatGame.Activities
{
    [Activity(ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class TeamsActivity : Activity, LoaderManager.ILoaderCallbacks
    {
        TeamsCursorAdapter tcAdapter;
        private static ManualResetEvent dbRefillResetEvent = new ManualResetEvent(false);

        private int teams;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Teams);

            ListView lvTeams = FindViewById<ListView>(Resource.Id.lvTeams);
            Button btnNext = FindViewById<Button>(Resource.Id.btnNext);
            btnNext.Click += BtnNext_Click;
            
            Context mContext = Application.Context;
            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(mContext);
            GameStates gameState = (GameStates)prefs.GetInt("GAME_STATE", 0);
            if (gameState != GameStates.EditingTeams)
            {
                teams = prefs.GetInt("TEAMS_COUNT", Resources.GetInteger(Resource.Integer.DEFAULT_TEAMS_COUNT));
                
                ISharedPreferencesEditor ed = prefs.Edit();
                ed.PutInt("GAME_STATE", (int)GameStates.EditingTeams);
                ed.Commit();

                ThreadPool.QueueUserWorkItem(c => { RefillDatabase(); dbRefillResetEvent.Set(); });
            }
            else
            {
                dbRefillResetEvent.Set();
            }

            tcAdapter = new TeamsCursorAdapter(this, null, false);
            tcAdapter.ReloadRequired += TcAdapter_ReloadRequired;

            lvTeams.Adapter = tcAdapter;
            LoaderManager.InitLoader(0, null, this);
        }

        private void RefillDatabase()
        {
            var gameDB = new GameDB(this);
            gameDB.Open();
            gameDB.ClearTeams();
            for (int i = 1; i <= teams; i++)
            {
                gameDB.AddTeam(string.Format("{0} {1}", GetString(Resource.String.Team), i));
            }
            gameDB.Close();
        }

        private void TcAdapter_ReloadRequired(object sender, EventArgs e)
        {
            LoaderManager.RestartLoader(0, null, this);
        }

        private void BtnNext_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(WordsActivity));
            StartActivity(intent);
            Finish();
        }

        public Loader OnCreateLoader(int id, Bundle args)
        {
            dbRefillResetEvent.WaitOne();
            return new TeamsCursorLoader(this);
        }

        public void OnLoaderReset(Loader loader)
        {
        }

        public void OnLoadFinished(Loader loader, Java.Lang.Object data)
        {
            tcAdapter.SwapCursor((ICursor)data);
        }
    }
}