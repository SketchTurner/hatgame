using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Preferences;
using System.Threading;

namespace HatGame.Activities
{
    [Activity(ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class WordsActivity : Activity
    {
        private int totalWords;
        private int presentWords;
        private int PresentWords
        {
            get { return presentWords; }
            set { presentWords = value; pbWords.Progress = presentWords; }
        }

        private GameDB gameDB;
        private ProgressBar pbWords;
        private EditText editWord;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Words);

            pbWords = FindViewById<ProgressBar>(Resource.Id.pbWords);
            Button btnAdd = FindViewById<Button>(Resource.Id.btnAdd);
            editWord = FindViewById<EditText>(Resource.Id.editWord);
            btnAdd.Click += BtnAdd_Click;
            editWord.EditorAction += EditWord_EditorAction;

            ThreadPool.QueueUserWorkItem(c => LoadData());
        }

        private void LoadData()
        {
            Context mContext = Android.App.Application.Context;
            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(mContext);
            GameStates gameState = (GameStates)prefs.GetInt("GAME_STATE", 0);
            int players = prefs.GetInt("PLAYERS_COUNT", Resources.GetInteger(Resource.Integer.DEFAULT_PLAYERS_COUNT));
            int words = prefs.GetInt("WORDS_COUNT", Resources.GetInteger(Resource.Integer.DEFAULT_WORDS_COUNT));
            totalWords = players * words;

            gameDB = new GameDB(this);
            gameDB.Open();

            if (gameState != GameStates.AddingWords)
            {
                ISharedPreferencesEditor ed = prefs.Edit();
                ed.PutInt("GAME_STATE", (int)GameStates.AddingWords);
                ed.Commit();
                gameDB.ClearWords();
            }

            int presentWordsCount = gameDB.GetWords�ursor().Count;

            RunOnUiThread(() =>
            {
                pbWords.Max = totalWords;
                PresentWords = presentWordsCount;
            });
        }

        private void EditWord_EditorAction(object sender, TextView.EditorActionEventArgs e)
        {
            if (e.ActionId == Android.Views.InputMethods.ImeAction.Next)
            {
                AddWord();
            }
        }

        protected override void OnResume()
        {
            base.OnResume();
            Window.SetSoftInputMode(SoftInput.StateVisible | SoftInput.AdjustResize);
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            AddWord();
        }

        private void AddWord()
        {
            
            string name = editWord.Text;
            if (String.IsNullOrWhiteSpace(name))
            {
                return;
            }
            ThreadPool.QueueUserWorkItem(c => gameDB.AddWord(name));

            PresentWords++;

            if (PresentWords < totalWords)
            {
                editWord.Text = String.Empty;
            }
            else
            {
                Intent intent = new Intent(this, typeof(GameplayActivity));
                StartActivity(intent);
                Finish();
            }
            
        }

        protected override void OnDestroy()
        {
            gameDB?.Close();
            base.OnDestroy();
        }
    }
}