﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Preferences;

namespace HatGame.Activities
{
    [Activity(Label = "@string/ApplicationName", MainLauncher = true, Icon = "@drawable/icon",
        ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class MainActivity : Activity
    {
        private GameStates gameState;
        private bool soundOn;

        private Button btnResume, btnNewGame, btnRules;
        private ImageButton btnSound;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.Main);

            btnResume = FindViewById<Button>(Resource.Id.btnResume);
            btnNewGame = FindViewById<Button>(Resource.Id.btnNewGame);
            btnRules = FindViewById<Button>(Resource.Id.btnRules);
            btnSound = FindViewById<ImageButton>(Resource.Id.btnSound);

            btnResume.Click += BtnResume_Click;
            btnNewGame.Click += BtnNewGame_Click;
            btnSound.Click += BtnSound_Click;
        }

        protected override void OnResume()
        {
            base.OnResume();

            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(Application.Context);
            gameState = (GameStates)prefs.GetInt("GAME_STATE", 0);
            soundOn = prefs.GetBoolean("SOUND_ON", true);
            SetSoundImage();
            bool isGameActive = gameState != GameStates.Inactive;

            btnResume.Visibility = isGameActive ? ViewStates.Visible : ViewStates.Invisible;
        }

        protected override void OnPause()
        {
            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(Application.Context);
            var editor = prefs.Edit();
            editor.PutBoolean("SOUND_ON", soundOn);
            editor.Commit();
            base.OnPause();
        }

        private void SetSoundImage()
        {
            if (soundOn)
            {
                btnSound.SetImageResource(Resource.Drawable.ic_volume_up_black_48dp);
            }
            else
            {
                btnSound.SetImageResource(Resource.Drawable.ic_volume_off_black_48dp);
            }
        }

        private void BtnSound_Click(object sender, EventArgs e)
        {
            soundOn = !soundOn;
            SetSoundImage();
        }

        private void BtnResume_Click(object sender, EventArgs e)
        {
            Type nextActivity;
            switch (gameState)
            {
                case GameStates.EditingTeams:
                    nextActivity = typeof(TeamsActivity);
                    break;
                case GameStates.AddingWords:
                    nextActivity = typeof(WordsActivity);
                    break;
                default:
                    nextActivity = typeof(GameplayActivity);
                    break;
            }
            Intent intent = new Intent(this, nextActivity);
            StartActivity(intent);
        }

        private void BtnNewGame_Click(object sender, EventArgs e)
        {
            if (gameState == GameStates.Inactive)
            {
                StartGame();
                return;
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.SetTitle(Resource.String.dialog_newgame_title);
            builder.SetMessage(Resource.String.dialog_newgame_message);
            builder.SetCancelable(true);

            builder.SetPositiveButton(Android.Resource.String.Yes, (mSender, args) => {
                (mSender as AlertDialog).Dismiss();
                StartGame();
            });

            builder.SetNegativeButton(Android.Resource.String.No, (mSender, args) => {
                (mSender as AlertDialog).Dismiss();
            });

            AlertDialog dialog = builder.Create();
            dialog.Show();
        }

        private void StartGame()
        {
            Intent intent = new Intent(this, typeof(NewGameActivity));
            StartActivity(intent);
        }
    }
}

