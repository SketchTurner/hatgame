using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Preferences;

namespace HatGame.Activities
{
    [Activity(ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class NewGameActivity : Activity
    {
        private TextView tvPlayers, tvWords, tvSeconds;
        private Spinner spinTeams;

        private int Players { get { return players; } set { players = value; tvPlayers.Text = players.ToString(); } }
        private int Teams { get { return teams; } set { teams = value; } }
        private int Words { get { return words; } set { words = value; tvWords.Text = words.ToString(); } }
        private int Seconds { get { return seconds; } set { seconds = value; tvSeconds.Text = seconds.ToString(); } }

        private int players, teams, words, seconds;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.NewGame);

            SeekBar seekPlayers = FindViewById<SeekBar>(Resource.Id.seekPlayers);
            SeekBar seekWords = FindViewById<SeekBar>(Resource.Id.seekWords);
            SeekBar seekSeconds = FindViewById<SeekBar>(Resource.Id.seekSeconds);
            spinTeams = FindViewById<Spinner>(Resource.Id.spinTeamsCount);
            tvPlayers = FindViewById<TextView>(Resource.Id.tvPlayersCount);
            tvWords = FindViewById<TextView>(Resource.Id.tvWordsCount);
            tvSeconds = FindViewById<TextView>(Resource.Id.tvSecondsCount);
            Button btnNext = FindViewById<Button>(Resource.Id.btnNext);

            Players = Resources.GetInteger(Resource.Integer.DEFAULT_PLAYERS_COUNT);
            Teams = Resources.GetInteger(Resource.Integer.DEFAULT_TEAMS_COUNT);
            Words = Resources.GetInteger(Resource.Integer.DEFAULT_WORDS_COUNT);
            Seconds = Resources.GetInteger(Resource.Integer.DEFAULT_SECONDS_COUNT);

            spinTeams.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(spinTeams_ItemSelected);
            var adapter = new ArrayAdapter<int>(this, Android.Resource.Layout.SimpleSpinnerItem);
            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            spinTeams.Adapter = adapter;
            RepopulateSpinner();
            SetSpinnerValue(Resources.GetInteger(Resource.Integer.DEFAULT_TEAMS_COUNT));

            seekPlayers.ProgressChanged += delegate {
                Players = seekPlayers.Progress + Resources.GetInteger(Resource.Integer.MIN_PLAYERS_COUNT);
                RepopulateSpinner();
            };
            seekWords.ProgressChanged += delegate {
                Words = seekWords.Progress + Resources.GetInteger(Resource.Integer.MIN_WORDS_COUNT);
            };
            seekSeconds.ProgressChanged += delegate {
                Seconds = seekSeconds.Progress + Resources.GetInteger(Resource.Integer.MIN_SECONDS_COUNT);
            };

            btnNext.Click += delegate
            {
                ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(Application.Context);
                ISharedPreferencesEditor ed = prefs.Edit();
                ed.PutInt("GAME_STATE", (int)GameStates.Inactive);
                ed.PutInt("PLAYERS_COUNT", players);
                ed.PutInt("WORDS_COUNT", words);
                ed.PutInt("SECONDS_COUNT", seconds);
                ed.PutInt("TEAMS_COUNT", teams);
                ed.PutInt("CURRENT_TEAM", 0);
                ed.PutInt("CURRENT_ROUND", 0);
                ed.Commit();

                Intent intent = new Intent(this, typeof(TeamsActivity));
                StartActivity(intent);
                Finish();
            };
        }

        private void spinTeams_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            Spinner spinner = (Spinner)sender;
            Teams = (int)spinner.GetItemAtPosition(e.Position);
        }

        private void RepopulateSpinner()
        {
            var items = new List<int>();
            int minValue = Resources.GetInteger(Resource.Integer.MIN_TEAMS_COUNT);
            int maxValue = Players / 2;

            for (int i = minValue; i <= maxValue; i++)
            {
                items.Add(i);
            }
            var adapter = spinTeams.Adapter as ArrayAdapter<int>;
            adapter.Clear();
            adapter.AddAll(items);
        }

        private void SetSpinnerValue(int value)
        {
            int pos = (spinTeams.Adapter as ArrayAdapter<int>).GetPosition(value);
            if (pos == -1)
            {
                pos = 0;
            }
            spinTeams.SetSelection(pos);
        }
    }
}