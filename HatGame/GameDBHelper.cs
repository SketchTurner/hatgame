using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Database.Sqlite;

namespace HatGame
{
    class GameDBHelper : SQLiteOpenHelper
    {
        public static readonly string create_table_teams_sql =
            "CREATE TABLE [teams] ([_id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE, [name] TEXT NOT NULL UNIQUE check(length(name) <= 20), [points] INTEGER)";
        public static readonly string create_table_words_sql =
            "CREATE TABLE [words] ([_id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE, [name] TEXT NOT NULL check(length(name) <= 40), [currentRound] INTEGER)";
        public static readonly string DbName = "hat_game.db";
        public static readonly int DbVersion = 1;

        public GameDBHelper(Context context) : base(context, DbName, null, DbVersion) { }

        public override void OnCreate(SQLiteDatabase db)
        {
            db.ExecSQL(create_table_teams_sql);
            db.ExecSQL(create_table_words_sql);
        }

        public override void OnUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
        {
            db.ExecSQL("drop table if exists teams");
            db.ExecSQL("drop table if exists words");
            OnCreate(db);
        }
    }
}