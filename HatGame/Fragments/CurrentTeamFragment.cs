using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Preferences;

namespace HatGame.Fragments
{
    public class CurrentTeamFragment : Fragment
    {
        private TextView tvRoundNumber, tvRoundName, tvCurrentTeamName;
        private Button btnNext;

        private GameDB gameDB;
        private List<Team> teams;
        private string[] roundNames;
        private int currentTeam, currentRound;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            gameDB = new GameDB(Application.Context);
            gameDB.Open();
            teams = gameDB.GetTeamsList();
            
            roundNames = Resources.GetStringArray(Resource.Array.RoundNames);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.CurrentTeam, container, false);

            tvRoundNumber = view.FindViewById<TextView>(Resource.Id.tvRoundNumber);
            tvRoundName = view.FindViewById<TextView>(Resource.Id.tvRoundName);
            tvCurrentTeamName = view.FindViewById<TextView>(Resource.Id.tvCurrentTeamName);
            btnNext = view.FindViewById<Button>(Resource.Id.btnNext);
            btnNext.Click += BtnNext_Click;

            return view;
        }

        public override void OnResume()
        {
            base.OnResume();

            Context ctx = Application.Context;
            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(ctx);
            GameStates gameState = (GameStates)prefs.GetInt("GAME_STATE", 0);
            currentTeam = prefs.GetInt("CURRENT_TEAM", 0);
            currentRound = prefs.GetInt("CURRENT_ROUND", 0);

            if (gameState != GameStates.Gameplay)
            {
                ISharedPreferencesEditor ed = prefs.Edit();
                ed.PutInt("GAME_STATE", (int)GameStates.Gameplay);
                ed.Commit();
            }
            if (currentRound < 3)
            {
                tvRoundNumber.Text = String.Format("{0} {1}", Resources.GetString(Resource.String.Round), currentRound + 1);
                tvRoundName.Text = roundNames[currentRound];
                tvCurrentTeamName.Text = teams[currentTeam].Name;
            }
        }

        private void BtnNext_Click(object sender, EventArgs e)
        {
            var moveFragment = new CurrentMoveFragment(teams[currentTeam].Id);
            var fTrans = Activity.FragmentManager.BeginTransaction();
            fTrans.Replace(Resource.Id.GameplayFragmentContainer, moveFragment);
            fTrans.AddToBackStack(null);
            fTrans.Commit();
        }

        public override void OnDestroy()
        {
            gameDB.Close();
            base.OnDestroy();
        }
    }
}