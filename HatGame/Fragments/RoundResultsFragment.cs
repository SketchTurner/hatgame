using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Preferences;
using HatGame.Activities;

namespace HatGame.Fragments
{
    public class RoundResultsFragment : Fragment
    {
        private Button btnNext;
        private ListView lvResults;
        private GameDB gameDB;
        private TeamsPointsAdapter teamsAdapter;

        private int currentRound;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.RoundResults, container, false);

            btnNext = view.FindViewById<Button>(Resource.Id.btnNext);
            lvResults = view.FindViewById<ListView>(Resource.Id.lvResults);

            btnNext.Click += BtnNext_Click;

            Context mContext = Application.Context;
            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(mContext);
            currentRound = prefs.GetInt("CURRENT_ROUND", 0);

            gameDB = new GameDB(Application.Context);
            gameDB.Open();

            var teams = gameDB.GetTeamsList();

            if (currentRound == 3)
            {
                btnNext.Text = Resources.GetString(Resource.String.GoToMainMenu);
                var sortedTeams = teams.OrderByDescending((t) => t.Points);
                string msg;
                if (sortedTeams.First().Points == sortedTeams.ElementAt(1).Points)
                {
                    msg = Resources.GetString(Resource.String.Draw);
                }
                else
                {
                    msg = String.Format("{0}{1}\"{2}\"", Resources.GetString(Resource.String.WinnerIs), System.Environment.NewLine, sortedTeams.First().Name);
                }
                var editFragment = new WinnerAlertFragment(msg);
                editFragment.Show(Activity.FragmentManager, "dialog");

                ISharedPreferencesEditor ed = prefs.Edit();
                ed.PutInt("GAME_STATE", (int)GameStates.Inactive);
                ed.Commit();
            }

            teamsAdapter = new TeamsPointsAdapter(Activity, teams);
            lvResults.Adapter = teamsAdapter;
            
            return view;
        }

        private void BtnNext_Click(object sender, EventArgs e)
        {
            if (currentRound < 3)
            {
                Activity.FragmentManager.PopBackStack();
            }
            else
            {
                Activity.Finish();
            }
        }

        public override void OnDestroy()
        {
            gameDB?.Close();
            base.OnDestroy();
        }
    }
}