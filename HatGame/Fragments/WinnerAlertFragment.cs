using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace HatGame.Fragments
{
    public class WinnerAlertFragment : DialogFragment
    {
        private TextView tvMessage;
        string message;

        public WinnerAlertFragment(string msg) : base()
        {
            message = msg;
        }
        
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var v = inflater.Inflate(Resource.Layout.WinnerAlert, container, false);
            tvMessage = v.FindViewById<TextView>(Resource.Id.tvWinnerMessage);
            tvMessage.Text = message;
            v.FindViewById<Button>(Resource.Id.btnOk).Click += btnOk_Click;
            return v;
        }

        public override Dialog OnCreateDialog(Bundle savedInstanceState)
        {
            var dialog = base.OnCreateDialog(savedInstanceState);
            dialog.Window.RequestFeature(WindowFeatures.NoTitle);
            return dialog;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Dismiss();
        }
    }
}