using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Preferences;

namespace HatGame.Fragments
{
    public class CurrentMoveFragment : Fragment, View.IOnTouchListener, GestureDetector.IOnGestureListener, GestureDetector.IOnDoubleTapListener
    {
        private GestureDetector _gestureDetector;
        private TextView tvCurrentWord;
        private ProgressBar pbTime;
        private LinearLayout layoutSkip;
        private FrameLayout layoutWord;
        private MoveCountDownTimer timer;
        private GameDB gameDB;

        private List<WordInfo> words;
        private List<long> wordIds;
        private WordInfo currentWord;
        private Random rand;
        private DateTime lastSwipeTime;

        private long millisUntilFinished = -1;
        private int prevRand;
        private int teamsCnt, secondsCnt;
        private int currentTeam, currentRound;
        private long teamId;
        private bool gameActive, wordSkipped;

        public CurrentMoveFragment(long teamId)
        {
            this.teamId = teamId;
            gameActive = false;
            wordSkipped = false;
            wordIds = new List<long>();
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            Context mContext = Application.Context;
            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(mContext);
            GameStates gameState = (GameStates)prefs.GetInt("GAME_STATE", 0);
            secondsCnt = prefs.GetInt("SECONDS_COUNT", Resources.GetInteger(Resource.Integer.DEFAULT_SECONDS_COUNT));
            teamsCnt = prefs.GetInt("TEAMS_COUNT", Resources.GetInteger(Resource.Integer.DEFAULT_TEAMS_COUNT));
            currentTeam = prefs.GetInt("CURRENT_TEAM", 0);
            currentRound = prefs.GetInt("CURRENT_ROUND", 0);
            prevRand = prefs.GetInt("PREVIOUS_RANDOM", -1);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.CurrentMove, container, false);
            pbTime = view.FindViewById<ProgressBar>(Resource.Id.pbTime);
            tvCurrentWord = view.FindViewById<TextView>(Resource.Id.tvCurrentWord);
            layoutSkip = view.FindViewById<LinearLayout>(Resource.Id.layoutSkip);
            layoutWord = view.FindViewById<FrameLayout>(Resource.Id.layoutWord);
            layoutSkip.Visibility = ViewStates.Invisible;
            pbTime.Max = secondsCnt * 5;
            view.SetOnTouchListener(this);
            return view;
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);
            view.SetOnTouchListener(this);
            _gestureDetector = new GestureDetector(this);
        }

        public override void OnPause()
        {
            timer?.Cancel();
            base.OnPause();
        }

        public override void OnResume()
        {
            base.OnResume();
            if (millisUntilFinished != -1)
            {
                StartTimer(millisUntilFinished);
            }
        }

        public override void OnDestroy()
        {
            gameDB?.Close();
            base.OnDestroy();
        }

        private void MarkKnown()
        {
            DateTime currTime = DateTime.Now;
            if (!gameActive || words.Count == 0 ||
                currTime.Subtract(lastSwipeTime).TotalMilliseconds < 1000)
            {
                return;
            }
            lastSwipeTime = currTime;
            wordIds.Add(currentWord.Id);
            words.Remove(currentWord);
            TakeRandomWord();
        }

        private void SkipWord()
        {
            if (!gameActive || wordSkipped)
            {
                return;
            }
            layoutSkip.Visibility = ViewStates.Invisible;
            wordSkipped = true;
            lastSwipeTime = DateTime.Now;
            TakeRandomWord();
        }

        private void StartGame()
        {
			if (gameActive)
			{
				return;
			}
            gameDB = new GameDB(Application.Context);
            gameDB.Open();
            words = gameDB.GetRoundWordsList(currentRound);
            layoutSkip.Visibility = ViewStates.Visible;
            layoutWord.Background = Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop ?
                Resources.GetDrawable(Resource.Drawable.RoundRect, Activity.Theme) :
#pragma warning disable CS0618 // GetDrawable(int) is deprecated in API 23, but is used in previous versions
                Resources.GetDrawable(Resource.Drawable.RoundRect);
#pragma warning restore CS0618 // 
            var g = Guid.NewGuid();
            var c = g.GetHashCode();
            rand = new Random(c);
            TakeRandomWord();
            StartTimer(secondsCnt * 1000);
            gameActive = true;
            lastSwipeTime = DateTime.Now;
        }

        private void StartTimer(long milliseconds)
        {
            timer = new MoveCountDownTimer(milliseconds, 200);
            timer.Tick += Timer_Tick;
            timer.Finish += Timer_Finish;
            timer.Start();
        }

        private void Timer_Finish()
        {
            pbTime.Progress = 0;
            Summarize();
            UpdatePreferences(false);
            Activity.FragmentManager.PopBackStack();
        }

        private void Timer_Tick(long millisUntilFinished)
        {
            this.millisUntilFinished = millisUntilFinished;
            pbTime.Progress = (int) (millisUntilFinished / 200);
        }

        private void TakeRandomWord()
        {
            int cnt = words.Count;
            if (cnt == 0)
            {
                InterruptIfEmpty();
                return;
            }

            int curRand;
            do
            {
                curRand = rand.Next() % cnt;
            }
            while (curRand == prevRand && cnt != 1);
            prevRand = curRand;

            currentWord = words[curRand];
            tvCurrentWord.Text = currentWord.Name;
        }

        private void Summarize()
        {
            gameDB.IncrementTeamPoints(teamId, wordIds.Count);
            gameDB.MoveWordsToNextRound(wordIds, currentRound + 1);
        }

        private void InterruptIfEmpty()
        {
            Summarize();

            timer.Cancel();

            Activity.FragmentManager.PopBackStack();
            var resultsFragment = new RoundResultsFragment();
            var fTrans = Activity.FragmentManager.BeginTransaction();
            fTrans.Replace(Resource.Id.GameplayFragmentContainer, resultsFragment, "results");
            fTrans.AddToBackStack(null);
            fTrans.Commit();

            UpdatePreferences(true);
        }

        private void UpdatePreferences(bool updateRound)
        {
            Context ctx = Application.Context;
            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(ctx);
            ISharedPreferencesEditor ed = prefs.Edit();
            ed.PutInt("CURRENT_TEAM", (currentTeam + 1) % teamsCnt);
            if (updateRound)
            {
                ed.PutInt("CURRENT_ROUND", currentRound + 1);
                ed.PutInt("PREVIOUS_RANDOM", prevRand);
            }
            ed.Commit();
        }

        public bool OnDown(MotionEvent e)
        {
            return false;
        }

        public bool OnFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
        {
            int SWIPE_THRESHOLD = 40;
            int SWIPE_VELOCITY_THRESHOLD = 40;
            float diffY = e2.GetY() - e1.GetY();
            float diffX = e2.GetX() - e1.GetX();
            if (Math.Abs(diffY) > Math.Abs(diffX))
            {
                if (Math.Abs(diffY) > SWIPE_THRESHOLD && Math.Abs(velocityY) > SWIPE_VELOCITY_THRESHOLD)
                {
                    MarkKnown();
                }
            }
            else
            {
                if (e1.GetY() >= layoutSkip.GetY() &&
                    Math.Abs(diffX) > SWIPE_THRESHOLD && Math.Abs(velocityX) > SWIPE_VELOCITY_THRESHOLD)
                {
                    SkipWord();
                }
            }
            return true;
        }

        public void OnLongPress(MotionEvent e)
        {
        }

        public bool OnScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
        {
            return false;
        }

        public void OnShowPress(MotionEvent e)
        {
        }

        public bool OnSingleTapUp(MotionEvent e)
        {
            return false;
        }

        public bool OnTouch(View v, MotionEvent e)
        {
            _gestureDetector.OnTouchEvent(e);
            return true;
        }

        public bool OnDoubleTap(MotionEvent e)
        {
            StartGame();
            return true;
        }

        public bool OnDoubleTapEvent(MotionEvent e)
        {
            return false;
        }

        public bool OnSingleTapConfirmed(MotionEvent e)
        {
            return false;
        }
    }
}