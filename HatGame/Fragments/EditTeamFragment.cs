using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Views.InputMethods;

namespace HatGame.Fragments
{
    public class EditTeamFragment : DialogFragment
    {
        EditText editTeam;

        public event EventHandler<TeamRenamedEventArgs> TeamRenamed;

        public static EditTeamFragment NewInstance(string oldName)
        {
            var frag = new EditTeamFragment();
            Bundle args = new Bundle();
            args.PutString("name", oldName);
            frag.Arguments = args;
            return frag;
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var v = inflater.Inflate(Resource.Layout.EditTeam, container, false);
            editTeam = v.FindViewById<EditText>(Resource.Id.editTeam);
            v.FindViewById<Button>(Resource.Id.btnSave).Click += BtnSave_Click;
            return v;
        }

        public override Dialog OnCreateDialog(Bundle savedInstanceState)
        {
            var dialog = base.OnCreateDialog(savedInstanceState);
            dialog.Window.RequestFeature(WindowFeatures.NoTitle);
            return dialog;
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            string newName = editTeam.Text;
            if (String.IsNullOrWhiteSpace(newName))
            {
                return;
            }
            OnTeamRenamed(new TeamRenamedEventArgs(newName));
            this.Dismiss();
        }

        protected virtual void OnTeamRenamed(TeamRenamedEventArgs e)
        {
            EventHandler<TeamRenamedEventArgs> handler = TeamRenamed;

            if (handler != null)
            {
                handler(this, e);
            }
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);
            Dialog.Window.SetSoftInputMode(SoftInput.StateVisible);
        }
    }

    public class TeamRenamedEventArgs : EventArgs
    {
        public TeamRenamedEventArgs(string n)
        {
            name = n;
        }
        private string name;

        public string Name
        {
            get { return name; }
        }
    }
}