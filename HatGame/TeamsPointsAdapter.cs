using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace HatGame
{
    class TeamsPointsAdapter : BaseAdapter<Team>
    {
        private List<Team> teams;
        Context ctx;
        LayoutInflater lInflater;

        public TeamsPointsAdapter (Context context, List<Team> teams)
        {
            ctx = context;
            this.teams = teams;
            lInflater = (LayoutInflater)ctx.GetSystemService(Context.LayoutInflaterService);
        }

        public override Team this[int position]
        {
            get
            {
                return teams[position];
            }
        }

        public override int Count
        {
            get
            {
                return teams.Count;
            }
        }

        public override long GetItemId(int position)
        {
            return teams[position].Id;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = convertView;
            if (view == null)
            {
                view = lInflater.Inflate(Resource.Layout.TeamsPointsListItem, parent, false);
            }
            Team t = this[position];
            
            view.FindViewById<TextView>(Resource.Id.tvTeamName).Text = t.Name;
            view.FindViewById<TextView>(Resource.Id.tvTeamPoints).Text = t.Points.ToString();

            return view;
        }
    }
}