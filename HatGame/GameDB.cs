using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Database.Sqlite;
using Android.Database;
using Android.Util;

namespace HatGame
{
    class GameDB
    {
        private Context mCtx;
        private GameDBHelper mDBHelper;
        private SQLiteDatabase mDB;
        private static readonly string TABLENAME_TEAMS = "teams";
        private static readonly string TABLENAME_WORDS = "words";
        private static readonly string COLUMN_NAME = "name";
        private static readonly string COLUMN_ROUND = "currentRound";

        public GameDB(Context ctx)
        {
            mCtx = ctx;
        }

        public void Open()
        {
            mDBHelper = new GameDBHelper(mCtx);
            mDB = mDBHelper.WritableDatabase;
        }

        public void Close()
        {
            if (mDBHelper != null) mDBHelper.Close();
        }

        public ICursor GetTeamsCursor()
        {
            return mDB.Query(TABLENAME_TEAMS, null, null, null, null, null, null);
        }

        public ICursor GetWords�ursor()
        {
            return mDB.Query(TABLENAME_WORDS, null, null, null, null, null, null);
        }

        public List<Team> GetTeamsList()
        {
            var cursor = GetTeamsCursor();
            List<Team> teams = new List<Team>();
            while (cursor.MoveToNext())
            {
                var team = new Team();
                team.Id = cursor.GetLong(0);
                team.Name = cursor.GetString(1);
                team.Points = cursor.GetInt(2);
                teams.Add(team);
            }
            return teams;
        }

        public List<WordInfo> GetRoundWordsList(int roundNumber)
        {
            var wordsCursor = mDB.Query(TABLENAME_WORDS, null, COLUMN_ROUND + " = ?",
                new string[] { roundNumber.ToString() }, null, null, null);
            var words = new List<WordInfo>();
            while (wordsCursor.MoveToNext())
            {
                var word = new WordInfo();
                word.Id = wordsCursor.GetLong(0);
                word.Name = wordsCursor.GetString(1);
                words.Add(word);
            }
            return words;
        }

        public void AddTeam(string name)
        {
            ContentValues cv = new ContentValues();
            cv.Put(COLUMN_NAME, name);
            mDB.Insert(TABLENAME_TEAMS, null, cv);
        }

        public void AddWord(string name)
        {
            ContentValues cv = new ContentValues();
            cv.Put(COLUMN_NAME, name);
            cv.Put(COLUMN_ROUND, 0);
            mDB.Insert(TABLENAME_WORDS, null, cv);
        }

        public void ClearTeams()
        {
            mDB.Delete(TABLENAME_TEAMS, null, null);
        }

        public void ClearWords()
        {
            mDB.Delete(TABLENAME_WORDS, null, null);
        }

        public bool RenameTeam(int id, string newName)
        {
            var cv = new ContentValues();
            cv.Put(COLUMN_NAME, newName);
            bool result = true;
            try
            {
                mDB.Update(TABLENAME_TEAMS, cv, "_id = " + id, null);
            }
            catch (SQLiteConstraintException ex)
            {
                Log.Error("HatGame", ex.Message);
                result = false;
            }
            return result;
        }

        internal void IncrementTeamPoints(long teamId, int value)
        {
            string[] whereArgs = new string[] { teamId.ToString() };
            var cursor = mDB.Query(TABLENAME_TEAMS, new string[] { "points" },
                "_id = ?", whereArgs, null, null, null);
            cursor.MoveToFirst();
            int currentPoints = cursor.GetInt(0);
            ContentValues cv = new ContentValues();
            cv.Put("points", currentPoints + value);
            mDB.Update(TABLENAME_TEAMS, cv, "_id = ?", whereArgs);
        }

        internal void MoveWordsToNextRound(List<long> wordIds, int nextRound)
        {
            foreach (long id in wordIds)
            {
                ContentValues cv = new ContentValues();
                cv.Put(COLUMN_ROUND, nextRound);
                mDB.Update(TABLENAME_WORDS, cv, "_id = ?", new string[] { id.ToString() });
            }
        }
    }
}