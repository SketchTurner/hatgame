using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Database;
using Android.Util;

namespace HatGame
{
    public class TeamsCursorAdapter : CursorAdapter
    {
        Activity _context;
        int idToChange;
        TextView tvToChange;

        public event EventHandler ReloadRequired;

        public TeamsCursorAdapter(Activity context, ICursor c, bool autoRequery = false)
            : base(context, c, autoRequery)
        {
            _context = context;
        }

        public override void BindView(View view, Context context, ICursor cursor)
        {
            var textView = view.FindViewById<TextView>(Resource.Id.tvTeamName);
            string name = cursor.GetString(1);
            int id = cursor.GetInt(0);
            textView.Text = name;
            var btnEdit = view.FindViewById<ImageButton>(Resource.Id.btnEditName);
            
            if (btnEdit.GetTag(Resource.Id.btnEditName) == null)
            {
                btnEdit.Click += delegate {
                    idToChange = (int)btnEdit.GetTag(Resource.Id.btnEditName);
                    tvToChange = textView;
                    RenameTeam();
                };
            }
            btnEdit.SetTag(Resource.Id.btnEditName, id);
        }

        public override View NewView(Context context, ICursor cursor, ViewGroup parent)
        {
            return _context.LayoutInflater.Inflate(Resource.Layout.TeamsListItem, parent, false);
        }

        private void RenameTeam()
        {
            string oldName = tvToChange.Text;
            var editFragment = Fragments.EditTeamFragment.NewInstance(oldName);
            editFragment.TeamRenamed += EditFragment_TeamRenamed;
            editFragment.Show(_context.FragmentManager, "dialog");
        }

        private void EditFragment_TeamRenamed(object sender, Fragments.TeamRenamedEventArgs e)
        {
            var db = new GameDB(_context);
            string newName = e.Name;
            db.Open();
            bool success = db.RenameTeam(idToChange, newName);
            db.Close();
            if (success)
            {
                OnReloadRequired();
            }
            else
            {
                Toast.MakeText(_context, _context.GetString(Resource.String.UnableToRenameTeam), ToastLength.Short).Show();
            }
            tvToChange = null;
        }

        protected virtual void OnReloadRequired()
        {
            EventHandler handler = ReloadRequired;

            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }
    }
}