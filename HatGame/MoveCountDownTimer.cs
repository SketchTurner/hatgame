using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace HatGame
{
    public delegate void TickEvent(long millisUntilFinished);
    public delegate void FinishEvent();

    class MoveCountDownTimer : CountDownTimer
    {
        public event TickEvent Tick;
        public event FinishEvent Finish;

        public MoveCountDownTimer(long totaltime, long interval) 
			: base(totaltime,interval)
		{
        }

        public override void OnFinish()
        {
            if (Finish != null)
                Finish();
        }

        public override void OnTick(long millisUntilFinished)
        {
            if (Tick != null)
                Tick(millisUntilFinished);
        }
    }
}