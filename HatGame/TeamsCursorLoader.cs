using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Database;

namespace HatGame
{
    class TeamsCursorLoader : CursorLoader
    {
        GameDB db;

        public TeamsCursorLoader(Context context) : base(context)
        {
            db = new GameDB(context);
        }

        public override Java.Lang.Object LoadInBackground()
        {
            db.Open();
            ICursor cursor = db.GetTeamsCursor();
            return (Java.Lang.Object)cursor;
        }

        protected override void OnStopLoading()
        {
            db.Close();
            base.OnStopLoading();
        }
    }
}